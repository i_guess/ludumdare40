var left = false;
var right = false;
var up = false;
var down = false;
var speed = 10;
var bugSpeed = 11;
var fps = 30;
var walks = false;
var faceRight = false;
var framesSinceSquish = 0;
var squishing = false;
var sloc = 0;
var programmingSpeed = 1;
var intervalId = 0;
var framesSinceBug = 0;


document.onclick = function(){
	removeText("introText");
	startExposition();
	document.onclick = function(){
		removeText("expo");
		continueExposition("He kind of sucks though.");
		document.onclick = function(){
			removeText("expo");
			continueExposition("So the more he codes...");
			document.onclick = function(){
				removeText("expo");
				continueExposition("The more bugs he has to squash.");
				document.onclick = function(){
					removeText("expo");
					continueExposition("Squash the bugs before...");
					document.onclick = function(){
						removeText("expo");
						continueExposition("they get to Guy or...");
						document.onclick = function(){
							removeText("expo");
							continueExposition("He'll stop programming.")
							document.onclick = function(){
								removeText("expo");
								continueExposition("Adjust his programming speed...");
								document.onclick =function(){
									removeText("expo");
									continueExposition("using the 1-3 keys, the faster he...");
									document.onclick = function(){
										removeText("expo");
										continueExposition("programs, the faster his lines of...");
										document.onclick = function(){
											removeText("expo");
											continueExposition("code grows.");
											document.onclick = function(){
												removeText("expo");
												continueExposition("Get as many lines as you can.");
												document.onclick = function(){
													removeText("expo");
													continueExposition("Move with the arrow keys.");
													intervalId = setInterval(gameLoop, 1000/fps);
													document.onclick =function(){
														removeText("expo");
														continueExposition("Squash with the space key.");
														document.onclick =function(){
															removeText("expo");
															removeText("instruction");
															document.onclick = function(){
																
															};
															gameStart();
														};
													};
												};
											};
										};
									};
									
								};
							};
						};
					};
				};
			};
		};
	};
}

document.onkeydown = function(ev){
	
	if(ev.keyCode == 27){
		document.onclick = function(){};
		console.log("skipped");
		let intro = document.getElementById("introText");
		if(intro){
			removeText("introText");
		}
		else{
			removeText("instruction");
			removeText("expo");
		}
		intervalId = setInterval(gameLoop, 1000/fps);
		gameStart();
	}
	checkControls(ev);
}

document.onkeyup = function(ev){
	if(ev.keyCode == 38){
		up = false;
	}
	else if(ev.keyCode == 40){
		down = false;
	}
	else if(ev.keyCode == 37){
		left = false;
	}
	else if(ev.keyCode == 39){
		right = false;
	}
}

function checkControls(ev){
	if(ev.keyCode == 38){
		up = true;
	}
	else if(ev.keyCode == 40){
		down = true;
	}
	else if(ev.keyCode == 37){
		left = true;
	}
	else if(ev.keyCode == 39){
		right = true;
	}
	else if(ev.keyCode == 32){
		addSquish();
	}
	else if(ev.keyCode == 49){
		programmingSpeed = 1;
	}
	else if (ev.keyCode == 50){
		programmingSpeed = 5;
	}
	else if(ev.keyCode == 51){
		programmingSpeed = 10;
	}
}

function gameStart(){
	document.onkeydown = function(ev){
			checkControls(ev);
	};
	startScore();
}

function gameLoop(){
	console.log(speed);
	var x = 0;
	var y = 0;
	if(left){
		x += -(speed/fps);
	}
	if(right){
		x += (speed/fps);
	}
	if(up){
		y += -(speed/fps);
	}
	if(down){
		y += (speed/fps);
	}
	let player = document.getElementsByClassName("player");
	for(var i = 0; i < player.length; i++){
		move(player[i], x, y);
	}
	if(squishing){
		framesSinceSquish += 100/fps;
		if(framesSinceSquish > fps){
			removeSquish();
		}
	}
	checkCollision();
	moveBugs();
	sloc += programmingSpeed/fps;
	
	if(Math.floor(sloc) % 5 == 0 && framesSinceBug > fps){
		addBug();
		framesSinceBug = 0;
	}
	else{
		framesSinceBug++;
	}
	
	keepScore();
}

function switchLeft(){
	faceRight = false;
	let allLeft = document.getElementsByClassName("leftFace");
	let allRight = document.getElementsByClassName("rightFace");
	
	for(var i = 0; i < allLeft.length; i++){
		allLeft[i].classList.remove("noShow");
	}
	for(var i = 0; i < allRight.length; i++){
		allRight[i].classList.add("noShow");
	}
}

function switchRight(){
	faceRight = true;
	let allLeft = document.getElementsByClassName("leftFace");
	let allRight = document.getElementsByClassName("rightFace");
	
	for(var i = 0; i < allLeft.length; i++){
		allRight[i].classList.remove("noShow");
	}
	for(var i = 0; i < allRight.length; i++){
		allLeft[i].classList.add("noShow");
	}
}

function startWalking(){
	walks = true;
	let standing = document.getElementsByClassName("standing");
	let walking = document.getElementsByClassName("walking");
	
	for(var i = 0; i < standing.length; i++){
		standing[i].classList.add("noShow");
	}
	for(var i = 0; i < walking.length; i++){
		walking[i].classList.remove("noShow");
	}
}

function stopWalking(){
	walks = false;
	let standing = document.getElementsByClassName("standing");
	let walking = document.getElementsByClassName("walking");
	
	for(var i = 0; i < standing.length; i++){
		standing[i].classList.remove("noShow");
	}
	for(var i = 0; i < walking.length; i++){
		walking[i].classList.add("noShow");
	}
}

function move(node, x, y){
	if(x != 0){
		node.setAttribute("cx", Number(node.getAttribute("cx"))+x);
	}
	if(y != 0){
		node.setAttribute("cy", Number(node.getAttribute("cy"))+y);
	}
	checkWalk(x, y);
	checkRight(x);
}

function checkWalk(x, y){
	if((x != 0 || y != 0) && !walks){
		startWalking();
	}
	if(walks && x == 0 && y == 0){
		stopWalking();
	}
}

function checkRight(x){
	if(x < 0 && faceRight){
		switchLeft();
	}
	if(x > 0 && !faceRight){
		switchRight();
	}
}

function removeText(textId){
	let child = document.getElementById(textId);
	child.parentNode.removeChild(child);
}

function startExposition(){
	let expo = '<text id="expo" x="0" y="80" font-size="7">This is Guy. He\'s a programmer.</text>';
	let partTwo = '<text id="instruction" x="0" y="90" font-size="4">click to continue, esc to skip.</text>'
	document.getElementById("textPlace").insertAdjacentHTML("beforeEnd", expo);
	document.getElementById("textPlace").insertAdjacentHTML("beforeEnd", partTwo);
}

function continueExposition(nexpo){
	let expo = '<text id="expo" x="0" y="80" font-size="7">'+nexpo+'</text>';
	document.getElementById("textPlace").insertAdjacentHTML("beforeEnd", expo);

}

function addBug(){
	
	var x = Math.random() * 100;
	var y = Math.random() * 100;
	
	let body = document.getElementById("body");
	let bx = Number(body.getAttribute("cx"))
	let by = Number(body.getAttribute("cy"));
	
	if(Math.abs(x - bx) < 4){
		x += 4;
	}
	if(Math.abs(y-by) < 4){
		y += 4;
	}
	
	let feetLvl = y+2.4;
	let rxFoot = x+2;
	let lxFoot = x-2;
	
	let rxEye = x+1.5;
	let lxEye = x-1.5;
	
	let bug = '<g class="aBug"><!-- feet -->' +
		'<circle class="bug bugRFoot" cx="'+rxFoot+'" cy="'+feetLvl+'" r="1" fill="purple" stroke="#3A003A" stroke-width="0.3"/>' +
		'<circle class="bug bugLFoot" cx="'+lxFoot+'" cy="'+feetLvl+'" r="1" fill="purple" stroke="#3A003A" stroke-width="0.3"/>' +
		'<!-- body -->' +
		'<circle class="bug bugBody" cx="'+x+'" cy="'+y+'" r="3" fill="purple" stroke="#3A003A" stroke-width="0.3"/>' +			
		'<!-- eyes -->' +
		'<circle class="bug" cx="'+rxEye+'" cy="'+y+'" r="1" fill="black" stroke="white" stroke-width="0.3"/>' +
		'<circle class="bug" cx="'+lxEye+'" cy="'+y+'" r="1" fill="black" stroke="white" stroke-width="0.3"/></g>';
	document.getElementById("bugPlace").insertAdjacentHTML("beforeEnd", bug)	
}

function squishedBug(bug){
	let toRemove = bug.parentNode;
	toRemove.parentNode.removeChild(toRemove);
}

function addSquish(){
	if(squishing){
		removeSquish();
	}
	
	let head = document.getElementById("head");
	var x = Number(head.getAttribute("cx"));
	var y = Number(head.getAttribute("cy"));
	
	if(faceRight && !up && !down){
		x += 10;
	}
	else if( !up && !down) {
		x -= 10;
	}
	else if( right){
		x += 10;
	}
	else if( left){
		x -= 10;
	}
	
	if(up){
		y -= 10;
	}
	if(down){
		y += 10;
	}
	
	let squash = '<g id="sandal" class="sandal">' +
			'<ellipse id="squish" class="squash" cx="'+x+'" cy="'+y+'" rx="5" ry="3" fill="tan" stroke="black" stroke-width="0.3"/>' +
			'<rect x="'+(x+0.5)+'" y="'+(y-2.8)+'" width="1" height="5.6" fill="black"/>' +
			'<rect x="'+(x+0.5)+'" y="'+y+'" width="4.4" height="1" fill="black"/>' +
			'</g>';
			
	document.getElementById("squashPlace").insertAdjacentHTML("beforeEnd", squash);
	squishing = true;
	squishSound();
}

function removeSquish(){
	
	let squish = document.getElementById("sandal");
	squish.parentNode.removeChild(squish);
	squishing = false;
	framesSinceSquish = 0;
}

function distanceFunc(x1, y1, x2, y2){
	return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));;
}

function checkCollision(){
	let bugs = document.getElementsByClassName("bugBody");
	var b_x;
	var b_y;
	var b_r;

	if(squishing){ 
		let mySquish = document.getElementById("squish");
		let s_x = Number(mySquish.getAttribute("cx"));
		let s_y = Number(mySquish.getAttribute("cy"));
		let s_r = Number(mySquish.getAttribute("rx"));
		var distance;
	
		for(var i = 0; i < bugs.length; i++){
			b_x = Number(bugs[i].getAttribute("cx"));
			b_y = Number(bugs[i].getAttribute("cy"));
			b_r = Number(bugs[i].getAttribute("r"));
			distance = distanceFunc(s_x, s_y, b_x, b_y);
			if(distance <= b_r + s_r){
				squishedBug(bugs[i]);
			}
		}
	}
	
	let player = document.getElementById("head");
	let p_x = Number(player.getAttribute("cx"));
	let p_y = Number(player.getAttribute("cy"));
	let p_r = Number(player.getAttribute("r"));
	var p_dist;
	
	for(var i = 0; i < bugs.length; i++){
		b_x = Number(bugs[i].getAttribute("cx"));
		b_y = Number(bugs[i].getAttribute("cy"));
		b_r = Number(bugs[i].getAttribute("r"));
		p_dist = distanceFunc(p_x, p_y, b_x, b_y);
		if(p_dist <= b_r + p_r){
			died();
		}
	}
	
}

function moveBugs(){
	let bugs = document.getElementsByClassName("bugBody");
	let me = document.getElementById("head");
	let p_x = Number(me.getAttribute("cx"));
	let p_y = Number(me.getAttribute("cy"));
	var b_x;
	var b_y;
	var change_x;
	var change_y;
	
	for(var i = 0; i < bugs.length; i++){
		b_x = Number(bugs[i].getAttribute("cx"));
		b_y = Number(bugs[i].getAttribute("cy"));
		change_x = 0;
		change_y = 0;
		
		let newSpeed =(bugSpeed/fps);
		
		if(p_x > b_x){
			change_x += newSpeed;
		}
		else if(p_x < b_x){
			change_x -= newSpeed;
		}
		
		if(p_y > b_y){
			change_y += newSpeed;
		}
		else if(p_y < b_y){
			change_y -= newSpeed;
		}
		moveBug(bugs[i], change_x, change_y);
	}
}

function moveBug(bug, x, y){
	
	let bugGroup = bug.parentNode;
	let bugChilds = bugGroup.children;
	for(var i = 0; i < bugChilds.length; i++){
		bugChilds[i].setAttribute("cx", Number(bugChilds[i].getAttribute("cx"))+x);
		bugChilds[i].setAttribute("cy", Number(bugChilds[i].getAttribute("cy"))+y);
	}
}

function died(){
	clearInterval(intervalId);
	deadScreen();
}

function startScore(){
	let text = document.getElementById("textPlace");
	text.insertAdjacentHTML("beforeEnd", '<text id="score" x="0" y="95" font-size="6">Lines of Code: 0</text>');
	text.insertAdjacentHTML("beforeEnd", '<text id="speed" x="0" y="99" font-size="4">Lines per second: '+programmingSpeed+'</text>');
}

function keepScore(){
	document.getElementById("score").innerHTML = "Lines of Code: "+ Math.floor(sloc);
	document.getElementById("speed").innerHTML = "Lines per second: " + programmingSpeed;
}

function squishSound(){
	document.getElementById("squishSound").play();
}

function deadScreen(){
	removeText("score");
	removeText("speed");
	let text = document.getElementById("finalText");
	text.insertAdjacentHTML("beforeEnd", '<text id="score" x="0" y="50" font-size="8">You got '+Math.floor(sloc)+' lines of code :)</text>');
	text.insertAdjacentHTML("beforeEnd", '<text id="score" x="0" y="70" font-size="6">Refresh the page to play again</text>');
}
